import { Room, Client } from "colyseus";
import { FourInRowState, MAX_PIECES_PER_CELL } from "./schema/FourInRowState";

export class FourInRowRoom extends Room<FourInRowState> {

  onCreate (options: any) {
    this.setState(new FourInRowState());

    this.onMessage("action", (client, message) => this.playerMove(client, message));
  }

  playerMove (client: Client, data: any) {
    if (this.state.winner || this.state.draw) {
      return false // game finished
    }
    if (client.sessionId !== this.state.currentTurn) {
      return false // not your turn
    }
    if (data.x >= this.state.board.size || data.y >= this.state.board.size) {
      return false // out of bounds
    }
    const cell = this.state.board.rows[data.x].cells[data.y]
    if (cell.pieces.length >= MAX_PIECES_PER_CELL) {
      return false // cell is full
    }

    const playerIds = Array.from(this.state.players.keys());
    const playerColor = playerIds.indexOf(client.sessionId)
    this.state.board.rows[data.x].cells[data.y].pieces.push(playerColor)
    
    if (this.state.board.hasWon(data.x, data.y)) {
      this.state.winner = client.sessionId;
    } else if (this.state.board.isComplete()) {
      this.state.draw = true;
    } else {
      // switch turn
      this.state.currentTurn = playerIds[(playerColor + 1) % playerIds.length];
    }
  }

  onJoin (client: Client, options: any) {
    this.state.players.set(client.sessionId, true);

    if (this.state.players.size === 2) {
      this.state.currentTurn = client.sessionId;
      // lock this room for new users
      this.lock();
    }
  }

  onLeave (client: Client, consented: boolean) {
    this.state.players.delete(client.sessionId);

    let remainingPlayerIds = Array.from(this.state.players.keys());
    if (!this.state.winner && !this.state.draw && remainingPlayerIds.length > 0) {
      this.state.winner = remainingPlayerIds.pop()
    }
  }

  onDispose() {}

}
