import { Schema, Context, type, ArraySchema, MapSchema } from "@colyseus/schema";

export const BOARD_SIZE = 4
export const MAX_PIECES_PER_CELL = 4
export const IN_A_ROW_TO_WIN = 4

class Cell extends Schema {
  @type(["number"]) pieces = new ArraySchema<number>()
}

class Column extends Schema {
  @type([Cell]) cells = new ArraySchema<Cell>()
}

class Board extends Schema {
  @type("number") size: number
  @type("number") maxPiecesPerCell: number
  @type([Column]) rows = new ArraySchema<Column>()

  constructor(size: number, maxPiecesPerCell: number) {
    super()
    this.size = size
    this.maxPiecesPerCell = maxPiecesPerCell
    for (let x = 0; x < this.size; x++ ) {
      const column = new Column()
      for (let y = 0; y < this.size; y++ ) {
        column.cells.push(new Cell())
      }
      this.rows.push(column)
    }
  }
  at(x: number, y: number, z: number) {
    if
    (
      x >= 0 && x < this.size &&
      y >= 0 && y < this.size &&
      z >= 0 && z < this.maxPiecesPerCell
    ) {
      const cell = this.rows[x].cells[y]
      if (z < cell.pieces.length) {
        return cell.pieces[z]
      }
    }
  }
  hasWon(x: number, y: number) {
    const lastPiece = {x, y, z: this.rows[x].cells[y].pieces.length - 1}
    const lastPieceColor:number = this.at(lastPiece.x, lastPiece.y, lastPiece.z)
    const vectors:Array<Vector> = [
      {x:  0, y:  0, z: -1},
      {x:  0, y: -1, z:  0},
      {x:  0, y: -1, z: -1},
      {x: -1, y:  0, z:  0},
      {x: -1, y:  0, z: -1},
      {x: -1, y: -1, z:  0},
      {x:  1, y: -1, z:  0},
      {x: -1, y: -1, z: -1},
      {x:  0, y:  1, z: -1},
      {x: -1, y:  1, z: -1},
      {x:  1, y:  0, z: -1},
      {x:  1, y: -1, z: -1},
      {x:  1, y:  1, z: -1}
    ]
    const transversing = (origin: Vector, vector: Vector, color: number):number => {
      const newPosition = {
        x: origin.x + vector.x,
        y: origin.y + vector.y,
        z: origin.z + vector.z,
      }
      if (this.at(newPosition.x, newPosition.y, newPosition.z) === color) {
        return 1 + transversing(newPosition, vector, color)
      }
      return 0
    }
    for (let i = 0; i < vectors.length; i++) {
      const vector = vectors[i]
      const opposite = { x: -vector.x, y: -vector.y, z: -vector.z }
      let in_a_row = 1
      in_a_row += transversing(lastPiece, vector, lastPieceColor)
      in_a_row += transversing(lastPiece, opposite, lastPieceColor)
      if (in_a_row >= IN_A_ROW_TO_WIN) {
        return true
      }
    }
    return false
  }
  isComplete() {
    for (let x = 0; x < this.size; x++ ) {
      for (let y = 0; y < this.size; y++ ) {
        if (this.rows[x].cells[y].pieces.length < this.maxPiecesPerCell) {
          return false
        }
      }
    }
    return true
  }
}

export class FourInRowState extends Schema {
  @type("string") currentTurn: string
  @type({ map: "boolean" }) players = new MapSchema<boolean>()
  @type(Board) board: Board = new Board(BOARD_SIZE, MAX_PIECES_PER_CELL)
  @type("string") winner: string;
  @type("boolean") draw: boolean;
}

interface Vector {
  x: number
  y: number
  z: number
}