import * as events from 'events'
import { Cell } from "components/cell"

@Component("board")
export class Board extends Entity {
  matrix: Array<Array<Cell>>
  pieceSize:number = 3

  constructor(
    size: number,
    piecesPerCell: number
  ){
    super()
    engine.addEntity(this)
    const boardSize:number = size * this.pieceSize
    const boardOrigin:number = -boardSize / 2
    this.matrix = new Array(size)
    for (let x = 0; x < size; x++) {
      this.matrix[x] = new Array(size)
      for (let y = 0; y < size; y++) {
        const cell = new Cell(x, y, piecesPerCell)
        cell.setParent(this)
        cell.addComponent(new Transform({
          position: new Vector3(
            boardOrigin + (x + .5) * this.pieceSize,
            0,
            boardOrigin + (y + .5) * this.pieceSize)
        }))
        this.matrix[x][y] = cell
      }
    }
    events.manager.addListener(events.GameTurn, this, (evnt) => {
      if (evnt.own) {
        this.enable()
      } else {
        this.disable()
      }
    })
    events.manager.addListener(events.GameEnd, this, () => {
      this.disable()
    })
  }
  protected mapCell(method: string): void {
    this.matrix.forEach((row) => {
      row.forEach((cell) => {
        cell[method]()
      })
    })
  }
  enable(): void {
    this.mapCell('enable')
  }
  disable(): void {
    this.mapCell('disable')
  }
  clear():void {
    this.mapCell('clear')
  }
  placePiece(x: number, y: number, player:number):void {
    this.matrix[x][y].placePiece(player)
  }
}