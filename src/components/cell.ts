import * as events from 'events'
import * as gfx from "resources/shapes"
import { Piece } from "components/piece"

export const CELL_HEIGHT = 1.15

@Component("cell")
export class Cell extends Entity {
  column: Array<Piece> = []
  x: number
  y: number
  size: number
  status: boolean
  pointer: OnPointerDown

  constructor(
    x: number,
    y: number,
    size: number
  ){
    super()
    engine.addEntity(this)
    this.x = x
    this.y = y
    this.size = size
    this.addComponent(gfx.Hole)
  }
  placePiece(player: number) {
    const piece = new Piece(player)
    engine.addEntity(piece)
    piece.setParent(this)
    piece.addComponent(new Transform({
      position: new Vector3(0, this.column.length * CELL_HEIGHT, 0)
    }))
    this.column.push(piece)
    if (this.column.length >= this.size) {
      this.disable()
    }
  }
  clear(): void {
    this.column.forEach((piece) => {
      engine.removeEntity(piece)
    })
    this.column = []
  }
  enable(): void {
    if (!this.status && this.column.length < this.size) {
      if (!this.pointer) {
        this.pointer = new OnPointerDown((e) => {
          events.manager.fireEvent(new events.PiecePlaced(this.x, this.y))
        }, {
          hoverText: "Place"
        })
      }
      this.status = true
      this.addComponentOrReplace(this.pointer)
    }
  }
  disable(): void {
    if (this.status) {
      this.status = false
      this.removeComponent(this.pointer)
    }
  }
}