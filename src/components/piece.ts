import * as gfx from "resources/shapes"

@Component("piece")
export class Piece extends Entity {
  player: number

  constructor(
    player: number
  ){
    super()
    this.player = player
    engine.addEntity(this)
    this.addComponent(player == 1 ? gfx.RosePiece : gfx.GreenPiece)
  }
}