import { NPC, NPCDelay, Dialog } from '@dcl/npc-scene-utils'

import * as events from 'events'

@Component("robot")
export class Robot extends Entity {
  robot: NPC
  in_game: boolean
  current: string

  constructor(
    position: TranformConstructorArgs
  ){
    super()
    engine.addEntity(this)
    this.robot = new NPC(
      position,
      'models/robots/marsha.glb',
      () => {
        const dialog = this.getDialog()
        if (!this.in_game) {
          this.robot.playAnimation('Hello', true, 2)
          this.addComponentOrReplace(
            new NPCDelay(2, () => {
              this.robot.playAnimation('Talk')
            })
          )
          this.robot.talk(dialog)
        } else {
          this.showBubble()
        }
      },
      {
        faceUser: true,
        onlyClickTrigger: true,
        portrait: {
          path: 'models/robots/marsha.png',
          height: 256,
          width: 256,
          section: {
            sourceHeight: 512,
            sourceWidth: 512,
          },
        }
      }
    )
    events.manager.addListener(events.ServerConnect, this, (evt) => {
      this.in_game = true
      this.current = 'connecting'
      this.showBubble()
    })
    events.manager.addListener(events.JoinedRoom, this, (evt) => {
      this.current = 'waiting_match'
      this.showBubble()
    })
    events.manager.addListener(events.GameTurn, this, (evt) => {
      this.current = evt.own ? 'your_turn' : 'opponent_turn'
      this.showBubble()
    })
    events.manager.addListener(events.GameEnd, this, (evt) => {
      const dialog = this.getDialog()
      this.robot.endInteraction()
      if (evt.draw) {
        this.robot.talk(dialog, 'game_draw')
      } else if (evt.won) {
        this.robot.talk(dialog, 'game_won')
      } else {
        this.robot.talk(dialog, 'game_lost')
      }
    })
  }
  showBubble(): void {
    this.robot.talkBubble([])
    this.robot.talkBubble(this.getDialog(), this.current)
  }
  restart(): void {
    this.robot.playAnimation('Idle')
    this.robot.endInteraction()
    events.manager.fireEvent(new events.ServerConnect())
  }
  getDialog(): Dialog[] {
    return [
      /* Introduction */
      {
        text: "Hi, I'm 4nRdy - welcome to this fun version of Connect 4 game!",
      },
      {
        text: 'Would you like to learn more about this game?',
        isQuestion: true,
        buttons: [
          { label: 'Yes', goToDialog: 3 },
          { label: 'No', goToDialog: 2 },
        ],
      },
      {
        text: "Okay, I'll be around if you get curious!",
        isEndOfDialog: true,
        triggeredByNext: () => {
          this.in_game = false
          this.robot.playAnimation('Goodbye', true, 2)
        },
      },
      {
        text: "Objective: To be the first player to connect 4 of the same colored blocks in a row (either vertically, horizontally, or diagonally)",
        skipable: true,
      },
      {
        text: "Players play in turns, and only one block can be dropped in each turn.",
        skipable: true,
      },
      {
        text: "On your turn, select the slot where you want to drop from the top one of your colored blocks.",
        skipable: true,
      },
      {
        text: "The game ends when there is a 4-in-a-row or a stalemate.",
        skipable: true,
      },
      {
        text: 'Would you like to join a game?',
        isQuestion: true,
        buttons: [
          {
            label: 'Yes',
            goToDialog: 5,
            triggeredActions: () => {
              this.restart()
            }
          },
          { label: 'No', goToDialog: 2 },
        ],
      },
      {
        text: 'Connecting to the server...',
        name: 'connecting',
        timeOn: Infinity
      },
      {
        text: 'Looking for an opponent...',
        name: 'waiting_match',
        timeOn: Infinity
      },
      {
        text: 'Your turn',
        name: 'your_turn',
        timeOn: Infinity
      },
      {
        text: 'Waiting for opponent move',
        name: 'opponent_turn',
        timeOn: Infinity
      },
      {
        text: 'It\'s a draw. Would you like to try again?',
        name: 'game_draw',
        isQuestion: true,
        buttons: [
          {
            label: 'Yes',
            goToDialog: 5,
            triggeredActions: () => {
              this.restart()
            }
          },
          { label: 'No', goToDialog: 2 },
        ],
      },
      {
        text: 'You won!!!. Would you like to try again?',
        name: 'game_won',
        isQuestion: true,
        buttons: [
          {
            label: 'Yes',
            goToDialog: 5,
            triggeredActions: () => {
              this.restart()
            }
          },
          { label: 'No', goToDialog: 2 },
        ],
      },
      {
        text: 'You lost :(. Would you like to try again?',
        name: 'game_lost',
        isQuestion: true,
        buttons: [
          {
            label: 'Yes',
            goToDialog: 5,
            triggeredActions: () => {
              this.restart()
            }
          },
          { label: 'No', goToDialog: 2 },
        ],
      },
    ]
  }
}