import { Client, Room } from "colyseus.js";
import { isPreviewMode, getCurrentRealm } from '@decentraland/EnvironmentAPI'
import { getUserData } from "@decentraland/Identity";
import * as events from 'events'

export async function connect(roomName: string, options: any = {}) {
    const isPreview = await isPreviewMode();
    const realm = await getCurrentRealm();

    options.realm = realm?.displayName;
    options.userData = await getUserData();

    const ENDPOINT = (isPreview)
        ? "ws://127.0.0.1:2567" // local environment
        : "wss://lxhhno.colyseus.de"; // production environment

    const client = new Client(ENDPOINT);
    const room = await client.joinOrCreate<any>(roomName, options);
    return room;
}
