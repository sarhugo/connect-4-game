export const manager = new EventManager()

export const GAME_WON = 1
export const GAME_LOST = -1
export const GAME_DRAW = 0

@EventConstructor()
export class ServerConnect {}

@EventConstructor()
export class JoinedRoom {}

@EventConstructor()
export class GameStarted {}

@EventConstructor()
export class GameTurn {
  public own: boolean

  constructor(own: boolean) {
    this.own = own;
  }
}

@EventConstructor()
export class PiecePlaced {
  public x: number
  public y: number

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }
}

@EventConstructor()
export class GameEnd {
  public draw: boolean = false
  public won: boolean = false

  constructor(result: number) {
    switch (result) {
      case GAME_DRAW:
        this.draw = true
        break
      case GAME_WON:
        this.won = true
        break
    }
  }
}
