export const Ground = new GLTFShape("models/FloorBaseTiles_01/FloorBaseTiles_01.glb")

export const Hole = new GLTFShape("models/hole.glb")

export const RosePiece = new GLTFShape("models/rose-piece.glb")
RosePiece.isPointerBlocker = false

export const GreenPiece = new GLTFShape("models/green-piece.glb")
GreenPiece.isPointerBlocker = false
