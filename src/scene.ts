import { Board } from 'components/board'
import { Robot } from 'components/robot'
import { connect } from 'connection'
import { Room } from "colyseus.js";
import * as gfx from 'resources/shapes'
import * as events from 'events'

@Component("scene")
export class Scene extends Entity {
  room: Room
  board: Board

  constructor(
    name?: string | undefined
  ){
    super(name)
    engine.addEntity(this)

    const ground = new Entity('ground')
    engine.addEntity(ground)
    ground.setParent(this)
    ground.addComponentOrReplace(gfx.Ground)
    ground.addComponentOrReplace(new Transform({
      position: new Vector3(8, 0, 8),
    }))

    const robot = new Robot({
      position: new Vector3(2.5, 1.5, 8),
      rotation: Quaternion.Euler(0, 180, 0),
    })

    events.manager.addListener(events.ServerConnect, this, async () => {
      this.room = await connect('4inRow')
      this.room.state.listen("currentTurn", (currentTurn) => {
        events.manager.fireEvent(new events.GameTurn(this.room.sessionId === currentTurn))
      })
      this.room.state.listen("draw", (draw) => {
        events.manager.fireEvent(new events.GameEnd(events.GAME_DRAW))
      })
      this.room.state.listen("winner", (winner) => {
        const result = this.room.sessionId === winner ? events.GAME_WON : events.GAME_LOST
        events.manager.fireEvent(new events.GameEnd(result))
      })
      this.room.state.players.onAdd = () => {
        if (this.room.state.players.size == 2) {
          events.manager.fireEvent(new events.GameStarted())
        } else {
          events.manager.fireEvent(new events.JoinedRoom())
        }
      }
      this.room.state.listen("board", (board) => {
        board.rows.onAdd = (row, x) => {
          row.cells.onAdd = (cell, y) => {
            cell.pieces.onAdd = (piece) => {
              this.board.placePiece(x, y, piece)
            }
          }
        }
      })
    })
    events.manager.addListener(events.PiecePlaced, this, (evt) => {
      this.room.send("action", { x: evt.x, y: evt.y })
    })
    events.manager.addListener(events.GameStarted, this, () => {
      if (!this.board) {
        this.board = new Board(this.room.state.board.size, this.room.state.board.maxPiecesPerCell)
        this.board.setParent(this)
        this.board.addComponentOrReplace(new Transform({
          position: new Vector3(8, 0, 8),
        }))
      } else {
        this.board.clear()
      }
      events.manager.fireEvent(new events.GameTurn(this.room.sessionId === this.room.state.currentTurn))
    })
  }
}